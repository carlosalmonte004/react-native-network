using ReactNative.Bridge;
using System;
using System.Collections.Generic;
using Windows.ApplicationModel.Core;
using Windows.UI.Core;

namespace Network.RNNetwork
{
    /// <summary>
    /// A module that allows JS to share data.
    /// </summary>
    class RNNetworkModule : NativeModuleBase
    {
        /// <summary>
        /// Instantiates the <see cref="RNNetworkModule"/>.
        /// </summary>
        internal RNNetworkModule()
        {

        }

        /// <summary>
        /// The name of the native module.
        /// </summary>
        public override string Name
        {
            get
            {
                return "RNNetwork";
            }
        }
    }
}
