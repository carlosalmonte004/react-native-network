
# react-native-network

## Getting started

`$ npm install react-native-network --save`

### Mostly automatic installation

`$ react-native link react-native-network`

### Manual installation


#### iOS

1. In XCode, in the project navigator, right click `Libraries` ➜ `Add Files to [your project's name]`
2. Go to `node_modules` ➜ `react-native-network` and add `RNNetwork.xcodeproj`
3. In XCode, in the project navigator, select your project. Add `libRNNetwork.a` to your project's `Build Phases` ➜ `Link Binary With Libraries`
4. Run your project (`Cmd+R`)<

#### Android

1. Open up `android/app/src/main/java/[...]/MainActivity.java`
  - Add `import com.reactlibrary.RNNetworkPackage;` to the imports at the top of the file
  - Add `new RNNetworkPackage()` to the list returned by the `getPackages()` method
2. Append the following lines to `android/settings.gradle`:
  	```
  	include ':react-native-network'
  	project(':react-native-network').projectDir = new File(rootProject.projectDir, 	'../node_modules/react-native-network/android')
  	```
3. Insert the following lines inside the dependencies block in `android/app/build.gradle`:
  	```
      compile project(':react-native-network')
  	```

#### Windows
[Read it! :D](https://github.com/ReactWindows/react-native)

1. In Visual Studio add the `RNNetwork.sln` in `node_modules/react-native-network/windows/RNNetwork.sln` folder to their solution, reference from their app.
2. Open up your `MainPage.cs` app
  - Add `using Network.RNNetwork;` to the usings at the top of the file
  - Add `new RNNetworkPackage()` to the `List<IReactPackage>` returned by the `Packages` method


## Usage
```javascript
import RNNetwork from 'react-native-network';

// TODO: What to do with the module?
RNNetwork;
```
  