
Pod::Spec.new do |s|
  s.name         = "RNNetwork"
  s.version      = "1.0.0"
  s.summary      = "RNNetwork"
  s.description  = <<-DESC
                  RNNetwork
                   DESC
  s.homepage     = "https://bitbucket.org/carlosalmonte004/react-native-network/src/master/"
  s.license      = "MIT"
  # s.license      = { :type => "MIT", :file => "FILE_LICENSE" }
  s.author             = { "author" => "author@domain.cn" }
  s.platform     = :ios, "7.0"
  s.source       = { :git => "https://github.com/author/RNNetwork.git", :tag => "master" }
  s.source_files  = "*.{h,m,swift}"
  s.requires_arc = true


  s.dependency "React"
  s.vendored_frameworks = [
    "frameworks/debug/AFNetworking.framework"
  ]

end

  